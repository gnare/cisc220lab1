# cisc220lab1

*NOTE:* I am using VSCode with WSL to compile C++. My projects will not look like the Eclipse projects we use in class. This is much easier for me to do (and likely more reliable) than having to try to deal with Eclipse with Makefiles. 

My labs should be able to Make natively on OSX and Linux, and with WSL or cygwin on Windows. I'll compile a Windows EXE just in case.
This will be under the Releases tab as well so I don't have to keep an executable file in the repo.