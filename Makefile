SRCS=$(wildcard *.cpp)
OBJS=$(SRCS:.cpp=.o )

artifact: lab.o;
	g++ $< -o $@


clean:
	-rm -f *.o
	-rm -f *.out
	-rm artifact


%.: %.cpp
	g++ -c $< -o $<.o


run: artifact
	./$<